//this will map who can do what based on their permissions which were stablished in the constants
import {actions, roles} from "../const/constants.js";

const mappings = new Map();

mappings.set(actions.CREAR_PROYECTO, [roles.ADMIN]);
mappings.set(actions.EDITAR, [roles.ADMIN]);
mappings.set(actions.REEMPLAZAR, [roles.ADMIN]);
mappings.set(actions.DESTRUIR, [roles.ADMIN]);
mappings.set(actions.LISTAR, [roles.ADMIN,roles.WORKER]);
mappings.set(actions.BUSCAR, [roles.ADMIN,roles.WORKER]);
mappings.set(actions.CERRAAR, [roles.ADMIN]);

export mappings;
