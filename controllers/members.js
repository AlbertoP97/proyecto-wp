const express = require('express');
const bcrypt = require('bcrypt');
const async = require('async');
const config = require('config');

const Member = require('../models/Member');

function blank(req, res, next){
    res.render('members/blank');
}

function create(req, res, next) {
    let fullName = req.body.fullName;
    let dob = req.body.dob;
    let curp = req.body.curp;
    let rfc = req.body.rfc;
    let address = req.body.address;
    let accesslvl = req.body.accesslvl;
    let skills = req.body.skills;

    async.parallel({
        salt:(callback) => {
            bcrypt.genSalt(10, callback)
        }
    }, (err, result) => {
        bcrypt.hash(curp, result.salt, (err, hash) => {

            let member = new Member({
                fullName: fullName,
                dob: dob,
                curp: curp,
                rfc: rfc,
                address: address,
                accesslvl: accesslvl,
                skills: skills,
                _salt: result.salt
            });

            member.save().then(obj => res.redirect('/members/')).catch(err => res.status(500).json({
                message: 'No se pudo almacenar el miembro',
                objs: err
            }));
        });
    });
}

function list(req, res, next){
    let page = req.params.page ? req.params.page : 1;
    Member.paginate({}, {page:page, limit:config.get('paginate.size')})
    .then((objs) => {
        console.log(objs);
        res.render('/members/list', {members:objs});
    }).catch(err => res.status(500).json({
        message: "No se pudieron encontrar los miembros del sistema",
        objs: err
    }));
}

function index(req, res, next){
    const id = req.params.id;
    Member.findOne({"_id": id}).then(obj => res.status(200).json({
        message: `Miembro del sistema con id: ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se pudieron encontrar los miembros del sistema",
        objs: err
    }));
}

function show(req, res, next){
    const id = req.params.id;

    User.findOne({"_id": id}).then(obj => res.render('members/edit', {member:obj}))
    .catch(err => res.status(500).json({
        message: "No se pudieron encontrar los miembros del sistema",
        objs: err
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    let fullName = req.body.fullName;
    let dob = req.body.dob;
    let curp = req.body.curp;
    let rfc = req.body.rfc;
    let address = req.body.address;
    let accesslvl = req.body.accesslvl;
    let skills = req.body.skills;

    let member = new Object();

    if(fullName){
        member._fullName = fullName;
    }

    if(dob){
        member._dob = dob;
    }

    if(curp){
        member._curp = curp;
    }

    if(rfc){
        member._rfc = rfc;
    }

    if(address){
        member._address = address;
    }

    if(address){
        member._accesslvl = accesslvl;
    }

    if(skills){
        member._skills = skills;
    }

    Member.findOneAndUpdate({"_id":id}, member).then(obj => res.status(200).json({
        message: `Miembro del sistema con id: ${id} se ha modificado`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se pudieron modificar los atributos del miembro del sistema",
        objs: err
    }));

}

function replace(req, res, next){
    const id = req.params.id;
    let fullName = req.body.fullName ? req.body.fullName : "";
    let dob = req.body.dob ? req.body.dob : "";
    let curp = req.body.curp ? req.body.curp : "";
    let rfc = req.body.rfc ? req.body.rfc : "";
    let address = req.body.address ? req.body.address : "";
    let accesslvl = req.body.accesslvl ? req.body.accesslvl : "";
    let skills = req.body.skills ? req.body.skills : "";


    let member = new Object({
        _fullName: fullName,
        _dob: dob,
        _curp: curp,
        _rfc: rfc,
        _address: address,
        _accesslvl: accesslvl,
        _skills: skills
    });

    Member.findOneAndReplace({"_id": id}, member).then(obj => res.status(200).json({
        mesage: `Se reemplazo el miembro del sistema con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se puede reemplazar el miembro",
        objs: err
    }));
}

function destroy(req, res, next){
    const id = req.params.id;
    Member.remove({"_id":id}).then(obj => res.status(200).json({
        mesage: `Se elimino el miembro del sistema con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se puede eliminar el miembro",
        objs: err
    }));
}

module.exports = {
    blank, create, list, index, show, edit, replace, destroy
}
