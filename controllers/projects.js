const express = require('express');

const Project = require('../models/proyecto');

//this is a temporal parameter to be solved
const currentuser = "WORKER"

//this functions first looks if the actions is valid and then looks if the user has permission
function hasPermission(role){
  if(role == "ADMIN"){
    return true;
  }
  else return false;
}

function create(req, res, next) {
    cancountinue = hasPermission(CREAR_PROYECTO, currentuser);
    if (cancountinue == false) {
      return;
    }

    let projname = req.body.projname;
    let rqstDate = req.body.rqstDate;
    let strtDate = req.body.strtDate;
    let description = req.body.description;
    let projectManager = req.body.projectManager;
    let owner = req.body.owner;
    let closed = req.body.closed;
    let devTeam = req.body.devTeam;

    let project = new Project({
        projname: projname,
        rqstDate: rqstDate,
        strtDate: strtDate,
        description: description,
        projectManager: projectManager,
        owner: owner,
        closed: false,
        devTeam: devTeam
    });

    project.save().then(obj => res.status(200).json({
        message: 'Proyecto creado correctamente',
        objs: obj
    })).catch(err => res.status(500).json({
        message: 'No se pudo almacenar el proyecto',
        objs: err
    }));

}



function list(req, res, next){
    let page = req.params.page ? req.params.page : 0;
    Project.find().then(objs => res.status(200).json({
        message: "Proyectos",
        objs: objs
    })).catch(err => res.status(500).json({
        message: "No se pudieron encontrar los proyectos",
        objs: err
    }));
}

function index(req, res, next){
    const id = req.params.id;
    Project.findOne({"_id": id}).then(obj => res.status(200).json({
        message: `Proyecto con id: ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se pudieron encontrar los proyectos",
        objs: err
    }));
}

function edit(req, res, next){
  cancountinue = hasPermission(currentuser);
  if (cancountinue == false) {
    return;
  }
    const id = req.params.id;
    let projname = req.body.projname;
    let rqstDate = req.body.rqstDate;
    let strtDate = req.body.strtDate;
    let description = req.body.description;
    let projectManager = req.body.projectManager;
    let owner = req.body.owner;
    let devTeam = req.body.devTeam;

    let project = new Object();

    if(projname){
        project._projname = projname;
    }

    if(rqstDate){
        project._rqstDate = rqstDate;
    }

    if(strtDate){
        project._strtDate = strtDate;
    }

    if(description){
        project._description = description;
    }

    if(projectManager){
        project._projectManager = projectManager;
    }

    if(owner){
        project._owner = owner;
    }


    if(devTeam){
        project._devTeam = devTeam;
    }

    Project.findOneAndUpdate({"_id":id}, project).then(obj => res.status(200).json({
        message: `El proyecto con id: ${id} se ha modificado`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se pudieron modificar los atributos del proyecto",
        objs: err
    }));

}

function replace(req, res, next){
  cancountinue = hasPermission(currentuser);
  if (cancountinue == false) {
    return;
  }
    const id = req.params.id;
    let projname = req.body.projname ? req.body.projname : "";
    let rqstDate = req.body.rqstDate ? req.body.rqstDate : "";
    let strtDate = req.body.strtDate ? req.body.strtDate : "";
    let description = req.body.description ? req.body.description : "";
    let projectManager = req.body.projectManager ? req.body.projectManager : "";
    let owner = req.body.owner ? req.body.owner : "";
    let devTeam = req.body.devTeam ? req.body.devTeam : "";

    let project = new Object({
        _projname: projname,
        _rqstDate: rqstDate,
        _strtDate: strtDate,
        _description: description,
        _projectManager: projectManager,
        _owner: owner,
        _closed: closed,
        _devTeam: devTeam
    });

    Project.findOneAndReplace({"_id": id}, project).then(obj => res.status(200).json({
        mesage: `Se reemplazo el proyecto con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se puede reemplazar el proyecto",
        objs: err
    }));
}

function destroy(req, res, next){
  cancountinue = hasPermission(currentuser);
  if (cancountinue == false) {
    return;
  }
    const id = req.params.id;
    Project.remove({"_id":id}).then(obj => res.status(200).json({
        mesage: `Se elimino el proyecto con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se puede eliminar el proyecto",
        objs: err
    }));
}

function close(req, res, next){
  cancountinue = hasPermission(currentuser);
  if (cancountinue == false) {
    return;
  }
    const id = req.params.id;
    let projname = req.body.projname;
    let rqstDate = req.body.rqstDate;
    let strtDate = req.body.strtDate;
    let description = req.body.description;
    let projectManager = req.body.projectManager;
    let owner = req.body.owner;
    let devTeam = req.body.devTeam;

    let project = new Object();

    if(projname){
        project._projname = projname;
    }

    if(rqstDate){
        project._rqstDate = rqstDate;
    }

    if(strtDate){
        project._strtDate = strtDate;
    }

    if(description){
        project._description = description;
    }

    if(projectManager){
        project._projectManager = projectManager;
    }

    if(owner){
        project._owner = owner;
    }

    if(closed){
        project._closed = true;
    }

    if(devTeam){
        project._devTeam = devTeam;
    }

    Project.findOneAndUpdate({"_id":id}, project).then(obj => res.status(200).json({
        message: `El proyecto con id: ${id} se ha cerrado`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se pudo cerrar",
        objs: err
    }));

}

module.exports = {
    create, list, index, edit, replace, destroy
}
