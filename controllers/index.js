const express = require('express');
const bcrypt = require('bcrypt');
const async = require('async');
const jwt = require('jsonwebtoken');
const config = require('config');

const User = require('../models/Member');

const jwtKey = config.get("secret.key");

function home(req,res,next){
  res.render('index', {title: 'Express', curp:false});
}

function dashboard(req, res, next){
  res.render('home');
}

function login(req,res,error){
  let fullName = req.body.fullName;
  let curp = req.body.curp

  async.parallel({
    user : callback => User.findOne({_fullName:fullName})
    .select('_curp _salt')
    .exec(callback)
    }, (err,result)=>{
      if(result.user){
        bcrypt.hash(curp, result.user.salt, (err, hash)=>{
          if(hash === result.user.curp){
            res.redirect('/home');
            res.status(200).json({
              "message": res.__('ok.login'),
              "objs": jwt.sign(res.user.dob, jwtKey)
            });
          }else{
            res.render('index', {title:'Express', curp:true})
          }
        });
      }else{
        res.render('index', {title:'Express', curp:true})
      }
  });
}

module.exports ={
  home, login, dashboard
}
