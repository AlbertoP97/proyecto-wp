const express = require('express');

const DevTeam = require('../models/DevTeam');

function create(req, res, next) {
    let members = req.body.members; 
    
    let devTeam = new DevTeam({
        members: members
    });

    devTeam.save().then(obj => res.status(200).json({
        message: 'Equipo de desarrollo creado correctamente',
        objs: obj
    })).catch(err => res.status(500).json({
        message: 'No se pudo almacenar el equipo de desarrollo',
        objs: err
    }));

}

function list(req, res, next){
    let page = req.params.page ? req.params.page : 0;
    DevTeam.find().then(objs => res.status(200).json({
        message: "Equipos de desarorllo",
        objs: objs
    })).catch(err => res.status(500).json({
        message: "No se pudieron encontrar los equipos de desarrollo",
        objs: err
    }));
}

function index(req, res, next){
    const id = req.params.id;
    DevTeam.findOne({"_id": id}).then(obj => res.status(200).json({
        message: `Equipo de desarrollo con id: ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se pudieron encontrar los equipos de desarrollo",
        objs: err
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    let members = req.body.members;
    
    let devTeam = new Object();
    
    if(members){
        devTeam._members = members;
    }

    DevTeam.findOneAndUpdate({"_id":id}, devTeam).then(obj => res.status(200).json({
        message: `Equipo de desarrollo con id: ${id} se ha modificado`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se pudieron modificar los atributos del equipo de desarrollo",
        objs: err
    }));

}

function replace(req, res, next){
    const id = req.params.id;
    let members = req.body.members ? req.body.members : "";

    let devTeam = new Object({
        _members: members
    });

    DevTeam.findOneAndReplace({"_id": id}, devTeam).then(obj => res.status(200).json({
        mesage: `Se reemplazo el equipo de desarrollo con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se puede reemplazar el equipo de desarrollo",
        objs: err
    }));
}

function destroy(req, res, next){
    const id = req.params.id;
    DevTeam.remove({"_id":id}).then(obj => res.status(200).json({
        mesage: `Se elimino el equipo de desarrollo con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se puede eliminar el equipo de desarrollo",
        objs: err
    }));
}

module.exports = {
    create, list, index, edit, replace, destroy
}
