const supertest = require('supertest');
const app = require('../app');

var key = "";

describe('Probar las rutas de los desarrolladores del equipo', () =>{
    it('Deberia de obtener la lista de los desarrolladores del equipo', (done) =>{
        supertest(app).get('/devTeam/')
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                //expect(res.body.objs.limit).toEqual(5);
                expect(res.statusCode).toEqual(200)
                //expect(res.body.message).equal("Desarrolladores del equipo")
                done();
            }
        });
    });
});