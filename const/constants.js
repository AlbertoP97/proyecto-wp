//possible actions that will be secured by permissions, these are limited to projects,
//however they can be made to secure other methods such as creating users, but due to time
//constrains this whill have to do
const actions = {
  CREAR_PROYECTO: "CREAR_PROYECTO",
  EDITAR: "EDITAR",
  REEMPLAZAR: "REEMPLAZAR",
  DESTRUIR: "DESTRUIR",
  LISTAR: "LISTAR",
  BUSCAR: "BUSCAR",
  CERRAR: "CERRAR"
}


//Based on these two basic roles one can decide who can create, edit, replace, destroy, find and list projects
// admin can do all while worker can only do the last two.
const roles ={
  ADMIN : "ADMIN",
  WORKER : "WORKER"
};
