const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _members : { type: mongoose.Schema.ObjectId, ref: "Member" }
});

class DevTeam{
    constructor(members){
      this._members = members;
    }

    get members(){
      return this._members;
    }
    set members(v){
      this._members = v;
    }
}

schema.loadClass(DevTeam);
module.exports = mongoose.model('DevTeam', schema);
