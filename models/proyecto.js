const mongoose = require('mongoose');

const schema = mongoose.Schema({
  _projname: String,
  _rqstDate: String,
  _strtDate: String,
  _description: String,
  _projectManager: String,
  _owner: String,
  _closed: String,
  _devTeam : { type: mongoose.Schema.ObjectId, ref: "DevTeam" }
});

class Project {
  constructor(projname,rqstDate, strtDate, description, projectManager, owner, devTeam){
    this._projname = projname;
    this._rqstDate = rqstDate;
    this._strtDate = strtDate;
    this._description = description;
    this._projectManager = projectManager;
    this._owner = owner;
    this._closed = closed;
    this._devTeam = devTeam;
  }

  get projname(){
    return this._projname;
  }

  set projname(v){
    this._projname = v;
  }

  get rqstDate(){
    return this._rqstDate;
  }

  set rqstDate(v){
    this._rqstDate = v;
  }

  get strtDate(){
    return this._strtDate;
  }

  set strtDate(v){
    this._strtDate = v;
  }

  get desctription(){
    return this._description;
  }

  set desctription(v){
    this._description = v;
  }

  get projectManager(){
    return this._projectManager;
  }

  set projectManager(v){
    this._projectManager = v;
  }

  get owner(){
    return this._owner;
  }

  set owner(v){
    this._owner = v;
  }

  get closed(){
    return this._closed;
  }

  set closed(v){
    this._closed = v;
  }

  get devTeam(){
    return this._devTeam;
  }

  set devTeam(v){
    this._devTeam = v;
  }
}

schema.loadClass(Project);
module.exports = mongoose.model('Project', schema);
