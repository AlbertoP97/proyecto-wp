const mongoose = require('mongoose');

const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
  _fullName : String,
  _dob: String,
  _curp: String,
  _rfc: String,
  _adress: String,
  _accesslvl: String,
  _skills: [String],
  _salt: String
});

class Member{
  constructor(fullName, dob, curp, rfc, address, accesslvl, skills, salt){
    this._fullName = fullName;
    this._dob = dob;
    this._curp = curp;
    this._rfc = rfc;
    this._address = address;
    this._accesslvl = accesslvl;
    this._skills = skills;
    this._salt = salt;
  }

  get fullName(){
    return this._fullName;
  }

  set fullName(v){
    this._fullName = v;
  }

  get dob(){
    return this._dob;
  }

  set fullName(v){
    this._dob = v;
  }

  get curp(){
    return this._curp;
  }

  set curp(v){
    this._curp = v;
  }

  get rfc(){
    return this._rfc;
  }

  set rfc(v){
    this._rfc = v;
  }

  get address(){
    return this._address;
  }

  set address(v){
    this._address = v;
  }
  
  get accesslvl(){
    return this._accesslvl;
  }

  set accesslvl(v){
    this._accesslvl = v;
  }

  get skills(){
    return this._skills;
  }

  set skills(v){
    this._skills = v;
  }

  get salt(){
    return this._salt;
  }

  set salt(v){
    this._salt = v;
  }
}

schema.loadClass(Member);
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('Member', schema);
